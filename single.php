<?php
/**
 * The blog template file.
 *
 * @package flatsome
 */

get_header();

?>

    <div id="content" class="blog-wrapper blog-single page-wrapper">
    <div class="row">

        <div class="breadcrumb">
            <?php echo bcn_display(); ?>

        </div>
        <div class="tag-category-single">
            <?php $category = get_the_category();

             ?>
            <ul>
                <?php
                if (!empty($category)) :
                    foreach ($category as $key => $item) : ?>
                        <li><a href="<?php echo get_category_link($item->term_id);?>" class="colorize-<?php echo $key;?>"><?php echo $item->name;?></a></li>
                    <?php endforeach;
                endif; ?>
            </ul>
        </div>
        <div class="col medium-10 content-left">
            <div class="meta-cs">
                <h1 id="title"><?php echo get_the_title(); ?></h1>
                <div class="info">
                    <a href="<?php echo get_permalink(); ?>" id="contentPlaceHolder_articleDetails_lnkAuthor"
                       class="author fader"></a>

                    <span class="date"><i class="fa fa-calendar-o" aria-hidden="true"></i> <?php echo get_the_date(); ?></span>

                    <span><i class=" fa fa-eye"
                             style="float: none; font-size:10px;"></i><?php echo postview_get(get_the_ID()); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;

                </div>
            </div>
            <?php get_template_part('template-parts/posts/single'); ?>
            <?php echo do_shortcode('[gs-fb-comments]'); ?>
        </div>
        <div class="post-sidebar large-2 col">
            <?php get_sidebar(); ?>
        </div><!-- .post-sidebar -->
    </div>

<?php get_footer();
