<?php
add_shortcode('thong_bao_qa', function ($atts) {
    extract(shortcode_atts(array(
        'title_qa' => '',
    ), $atts));
    ob_start();
    ?>
    <div class="row">
        <div class="col medium-12">
            <div class="announcement">
                <span class="title"><?php echo !empty($title_qa) ? $title_qa : 'Bài Viết Mới'; ?></span>
                <div class="ticker-container">
                    <ul>
                        <?php $postquery = new WP_Query(array('posts_per_page' => 10));
                        if ($postquery->have_posts()) {
                            while ($postquery->have_posts()) : $postquery->the_post();
                                ?>
                                <div>
                                    <li>
                                        <span><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></span>
                                    </li>
                                </div>
                            <?php endwhile;
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php
    return ob_get_clean();
});


//feature-articles-item
add_shortcode('articles_item_qa', function ($atts) {
    extract(shortcode_atts(array(
        'style' => '',
        'category' => ''
    ), $atts));
    $style = !empty($style)?$style:'style-1';
    ob_start(); ?>
    <?php if($style=='style-1') :?>
    <!-- style-1-->
    <div class="feature-articles-item">
        <h1><a href="/chuyen-muc/ky-nang-mem">Kỹ năng mềm</a></h1>
        <ul>

            <li>
                <div class="image-wrapper">
                    <a title="5 Cách Dễ Dàng Để Khám Phá Thiên Hướng Sáng Tạo Của Bạn"
                       href="/5-cach-de-dang-de-kham-pha-thien-huong-sang-tao-cua-ban~45320">
                        <img onerror="imgError(this);"
                             src="https://www.saga.vn/uploads/Thumbnail 2/creative_genius_Thumbnail.png"></a>
                </div>
                <a title="5 Cách Dễ Dàng Để Khám Phá Thiên Hướng Sáng Tạo Của Bạn"
                   href="/5-cach-de-dang-de-kham-pha-thien-huong-sang-tao-cua-ban~45320" class="title">5 Cách Dễ Dàng Để
                    Khám Phá Thiên Hướng Sáng Tạo Của Bạn</a>
                <span class="info"> 18/04/2019 - 07:00</span>
                <span class="teaser">Hôm nay, SAGA.VN sẽ giới thiệu cho bạn một infographic thú ...</span>
            </li>

            <li>
                <div class="image-wrapper">
                    <a title="Câu Chuyện Về Hai Lối Tư Duy: Cách Tư Duy Tăng Trưởng Giúp Bạn Cạnh Tranh Trong Môi Trường Làm Việc Thay Đổi"
                       href="/cau-chuyen-ve-hai-loi-tu-duy-cach-tu-duy-tang-truong-giup-ban-canh-tranh-trong~45276">
                        <img onerror="imgError(this);"
                             src="https://www.saga.vn/uploads/Thumbnail 2/two_mindsets_thumbnail.jpg"></a>
                </div>
                <a title="Câu Chuyện Về Hai Lối Tư Duy: Cách Tư Duy Tăng Trưởng Giúp Bạn Cạnh Tranh Trong Môi Trường Làm Việc Thay Đổi"
                   href="/cau-chuyen-ve-hai-loi-tu-duy-cach-tu-duy-tang-truong-giup-ban-canh-tranh-trong~45276"
                   class="title">Câu Chuyện Về Hai Lối Tư Duy: Cách Tư Duy Tăng Trưởng Giúp Bạn Cạnh Tranh Trong Môi
                    Trường Làm Việc Thay Đổi</a>
                <span class="info">Bởi Kim Anh -  20/03/2019 - 07:00</span>
                <span class="teaser">Khả năng thích ứng luôn đóng một vai trò qu ...</span>
            </li>

            <li>
                <div class="image-wrapper">
                    <a title="Cách Để Dẫn Dắt Câu Chuyện Như Những Diễn Giả Trên Ted Talks"
                       href="/cach-de-dan-dat-cau-chuyen-nhu-nhung-dien-gia-tren-ted-talks~43556">
                        <img onerror="imgError(this);"
                             src="https://www.saga.vn/uploads/thumbnail 300x300/Dan dat cau chuyen_Anh_thumbnail.png"></a>
                </div>
                <a title="Cách Để Dẫn Dắt Câu Chuyện Như Những Diễn Giả Trên Ted Talks"
                   href="/cach-de-dan-dat-cau-chuyen-nhu-nhung-dien-gia-tren-ted-talks~43556" class="title">Cách Để Dẫn
                    Dắt Câu Chuyện Như Những Diễn Giả Trên Ted Talks</a>
                <span class="info">Bởi Minh Ngô -  27/10/2018 - 07:00</span>
                <span class="teaser">Bạn có muốn có được bí quyết để thuyết trì ...</span>
            </li>

        </ul>
    </div>
    <?php endif; ?>
    <?php if($style=='style-2') :?>
    <!-- style-2-->
    <div class="focus box" id="bai-noi-bat-tt">
        <div class="box-content row">

            <div class="top col medium-6">
                <a href="/mo-hinh-tang-toc-khoi-nghiep-thuc-su-la-lam-gi~45359" class="invert">
                    <img src="https://www.saga.vn/uploads/Thumbnail 2/Startup-Accelerator-thumbnail.jpg">
                    <span class="top-title">Mô Hình Tăng Tốc Khởi Nghiệp Thực Sự Là Làm Gì?</span>
                    <span class="top-title hidden-sm" style="position: absolute;bottom: 0px;height: 25px;"></span>
                </a>
                <a><span class="tag tag-big">Bài Nổi Bật Trong Tuần</span></a>
                <div class="article-info clearfix hidden-sm">
                        <span class="view author" style="display:none">
                            <small>+ </small>
                        </span>
                    <span class="view">
                            <em><i class=" fa fa-eye" style="float: none; font-size:10px;"></i>103</em>
                        </span>

                </div>
            </div>


            <div class="articles col medium-6">
                <ul>

                    <li>
                        <a title="Quy Trình PHASE-GATE Là Gì?" href="/quy-trinh-phase-gate-la-gi~45641"
                           class="title invert">Quy Trình PHASE-GATE Là Gì?</a>
                        <div class="article-info info-sm clearfix hidden-sm">
                                <span class="view">
                                    <span>
                                        <em><i class=" fa fa-eye"
                                               style="float: none;  font-size:10px;"></i>78</em>
                                    </span>
                                </span>

                        </div>
                    </li>

                    <li>
                        <a title="Chửi Bậy Nơi Công Sở? Nên Hay Không Nên?"
                           href="/chui-bay-noi-cong-so-nen-hay-khong-nen~45347" class="title invert">Chửi Bậy Nơi Công
                            Sở? Nên Hay Không Nên?</a>
                        <div class="article-info info-sm clearfix hidden-sm">
                                <span class="view">
                                    <span>
                                        <em><i class=" fa fa-eye"
                                               style="float: none;  font-size:10px;"></i>224</em>
                                    </span>
                                </span>


                        </div>
                    </li>

                    <li>
                        <a title="Hướng Dẫn Chiến Lược Định Vị Thị Trường"
                           href="/huong-dan-chien-luoc-dinh-vi-thi-truong~45346" class="title invert">Hướng Dẫn Chiến
                            Lược Định Vị Thị Trường</a>
                        <div class="article-info info-sm clearfix hidden-sm">
                                <span class="view">
                                    <span>
                                        <em><i class=" fa fa-eye"
                                               style="float: none;  font-size:10px;"></i>226</em>
                                    </span>
                                </span>

                        </div>
                    </li>

                    <li>
                        <a title="Tiền Ảo Ethereum Sẽ Là “Xương Sống” Của Hình Thức Internet Mới"
                           href="/tien-ao-ethereum-se-la-xuong-song-cua-hinh-thuc-internet-moi~45358"
                           class="title invert">Tiền Ảo Ethereum Sẽ Là “Xương Sống” Của Hình Thức Internet Mới</a>
                        <div class="article-info info-sm clearfix hidden-sm">
                                <span class="view">
                                    <span>
                                        <em><i class=" fa fa-eye"
                                               style="float: none;  font-size:10px;"></i>252</em>
                                    </span>
                                </span>

                        </div>
                    </li>

                    <li>
                        <a title="Khoá Học Tiếng Anh Kinh Doanh Online Miễn Phí - SAGA.VN: Sales Và Marketing"
                           href="/khoa-hoc-tieng-anh-kinh-doanh-online-mien-phi-sagavn-sales-va-marketing~45712"
                           class="title invert">Khoá Học Tiếng Anh Kinh Doanh Online Miễn Phí - SAGA.VN: Sales Và
                            Marketing</a>
                        <div class="article-info info-sm clearfix hidden-sm">
                                <span class="view">
                                    <span>
                                        <em><i class=" fa fa-eye"
                                               style="float: none;  font-size:10px;"></i>237</em>
                                    </span>
                                </span>

                        </div>
                    </li>

                    <li>
                        <a title=" Hướng Dẫn Nâng Cao Cho Chiến Dịch Truyền Thông Online "
                           href="/huong-dan-nang-cao-cho-chien-dich-truyen-thong-online~45317" class="title invert">
                            Hướng Dẫn Nâng Cao Cho Chiến Dịch Truyền Thông Online </a>
                        <div class="article-info info-sm clearfix hidden-sm">
                                <span class="view">
                                    <span>
                                        <em><i class=" fa fa-eye"
                                               style="float: none;  font-size:10px;"></i>266</em>
                                    </span>
                                </span>

                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if($style=='style-3') :?>
    <!-- style-3-->
    <div class="box event">
        <div class="box-title color-2">
            <h3>Sự kiện</h3>
            <p></p></div>
        <div class="box-content">
            <div class="event-picture">
                <p><a href="/su-kien/rmit-vietnam-research-challenge~26">
                        <img class="top" src="https://www.saga.vn//uploads/Sự kiện/Artworks_Profile Pic.png">
                        <span>
                            RMIT Vietnam Research Challenge
                            03/03/2018
                        </span>
                    </a></p></div>
            <ul>
                <li>
                    <a href="/su-kien/meed-launching~23">
                        <img src="https://www.saga.vn//uploads/Sự kiện/MEED LOGO-01.jpg">
                        <span>Meed Launching</span>
                    </a>
                    22/11/2016
                </li>
                <li>
                    <a href="/su-kien/front-the-most-2016~22">
                        <img src="https://www.saga.vn/uploads/Sự kiện/FTM.png">
                        <span>Front The Most 2016</span>
                    </a>
                    09/05/2016
                </li>
            </ul>
            <div class="text-center">
                <a href="/su-kien" class="more">Xem thêm &gt;&gt;</a>
            </div>
            <p></p></div>
    </div>
    <?php endif; ?>
    <?php
    return ob_get_clean();
});