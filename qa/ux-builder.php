<?php
function devqa_ux_builder_element()
{
    add_ux_builder_shortcode('thong_bao_qa', array(
            'name' => __('Breaking News'),
            'category' => __('ITSWEB'),
            'thumbnail' => QA_PATCH . '/assets/images/logo.png',
            'priority' => 1,
            'options' => array(
                'title_qa' => array(
                    'type' => 'textfield',
                    'heading' => 'Tiêu đề',
                    'default' => 'Bài Viết Mới',
                )
            ))
    );
    // list post
    add_ux_builder_shortcode('articles_item_qa', array(
            'name' => __('Danh sách bài viết'),
            'category' => __('ITSWEB'),
            'thumbnail' => QA_PATCH . '/assets/images/logo.png',
            'priority' => 2,
            'options' => array(
                'style' => array(
                    'type' => 'radio-buttons',
                    'heading' => __('Style'),
                    'default' => 'outline',
                    'options' => array(
                        'style-1' => array('title' => 'Kiểu 1'),
                        'style-2' => array('title' => 'Kiểu 2'),
                        'style-3' => array('title' => 'Kiểu 3'),
                    ),
                ),
                'category' => array(
                    'type' => 'select',
                    'heading' => __('Chọn category', 'flatsome'),
                    'full_width' => true,
                    'config' => array(
                        'placeholder' => __('Select', 'flatsome'),
                        'termSelect' => array(
                            'post_type' => 'post',

                        ),
                    )
                ),
            ))
    );

}

add_action('ux_builder_setup', 'devqa_ux_builder_element');