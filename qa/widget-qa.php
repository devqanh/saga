<?php

// tin noi bat
class TinnoibatWidget extends WP_Widget
{
    public function __construct()
    {
        $widget_ops = array(
            'classname' => 'tinnoibat_widget',
            'description' => 'Tin Nôi Bật built with ACF Pro',
        );
        parent::__construct('tinnoibat_widget', 'Tin Nôi Bật', $widget_ops);
    }

    public function widget($args, $instance)
    {
        if (!isset($args['widget_id'])) {
            $args['widget_id'] = $this->id;
        }
        $widget_id = 'widget_' . $args['widget_id'];
        $posts_per_page = get_field('number_post', $widget_id);
        $title = get_field('title', $widget_id);
        $categories = get_the_category(get_the_ID());
        ob_start();
        if ($categories) :
            $category_ids = array();
            foreach ($categories as $individual_category) $category_ids[] = $individual_category->term_id;
            $args = array(
                'category__in' => $category_ids,
                'post__not_in' => array(get_the_ID()),
                'posts_per_page' => $posts_per_page, // So bai viet dc hien thi
            );
            $my_query = new wp_query($args);
            if ($my_query->have_posts()):
                ?>
                <div class="box effect2 border articles-list" id="bai-viet-noi-bat">
                    <div class="box-title color-6">
                        <h3><?php echo $title; ?></h3>
                    </div>
                    <div class="box-content">
                        <ul id="arc_ctl00_contentPlaceHolder_rightSidebar_ctl00" class="article-titles">
                            <?php while ($my_query->have_posts()):$my_query->the_post(); ?>
                                <li><a class="invert"
                                       href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
            <?php
            endif;
            wp_reset_query();
        endif;
        echo ob_get_clean();

    }

    public function form($instance)
    {
        // outputs the options form on admin
    }


    public function update($new_instance, $old_instance)
    {
        // processes widget options to be saved
    }
}

/**
 * Register our CTA Widget
 */
function register_prj_widget()
{
    register_widget('TinnoibatWidget');
}

add_action('widgets_init', 'register_prj_widget');
