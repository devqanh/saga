<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main><!-- #main -->

<footer id="footer" class="footer-wrapper">
    <div class="footer-container">
        <div class="container">
            <div class="row middle clearfix">
                <div class="col color-2">
                    <div class="inner first">
                        <img alt="logo footer" src="https://www.saga.vn/Assets/img/footer-logo.png"><br>
                        <span class="copyright">© 2019 SAGA.VN, All rights reserved. Powered by IPCOMS.</span>

                    </div>
                </div>
                <div class="col color-6">
                    <div class="inner">
                        <h4>Thông tin chung</h4>
                        <a href="http://www.saga.vn/ve-saga~42860">Về Saga</a>
                        <a href="http://www.saga.vn/saga-101~42861">Saga 101</a>
                        <a href="http://www.saga.vn/tag/blog">Saga Blog</a>
                        <a href="http://www.saga.vn/lien-he~42862">Liên hệ</a>
                        <a href="http://www.saga.vn/tag/tuyen-dung">Tuyển dụng</a>
                        <a href="http://www.saga.vn/so-do-trang~42864">Sơ đồ trang</a>

                    </div>
                </div>
                <div class="col color-4">
                    <div class="inner">
                        <h4>Xem thêm trên Saga</h4>
                        <a href="http://www.saga.vn/doi-tac-trang-doi-thong-tin~42865">Đối tác trao đổi thông tin</a>
                        <a href="http://www.saga.vn/goodies~42866">Goodies</a>
                        <a href="http://xaydungvietnam.vn/">Saga BUILD</a>
                        <a href="#">Saga Sức khỏe</a>
                        <a href="http://www.saga.vn/feed/rss-news">RSS</a>
                    </div>
                </div>
                <div class="col color-5">
                    <div class="inner">
                        <h4>Truyền thông</h4>
                        <a href="http://www.saga.vn/saga-tren-bao~42871">Saga trên báo</a>
                        <a href="http://www.saga.vn/saga-tren-bao~42871">Press Kit</a>
                        <h4>Quảng cáo trên Saga</h4>
                        <a href="http://www.saga.vn/bao-gia~42873">Báo giá</a>
                    </div>
                </div>
                <div class="col color-10">
                    <div class="inner">
                        <h4>Điều khoản</h4>
                        <a href="/dieu-khoan-dich-vu~42558">Điều khoản dịch vụ</a>
                        <a href="/chinh-sach-bao-mat~42563">Chính sách bảo mật</a>
                        <h4>Giấy phép</h4>
                        <a href="#">Hoạt động theo GP Thiết lập Trang thông tin Điện tử Tổng hợp số 1208/GP-TTĐT ngày
                            15/11/2011, do Sở TTTT Hà Nội cấp</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom text-center">
            SAGA.VN
        </div>
    </div>
    <?php //do_action('flatsome_footer'); ?>

</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<?php wp_footer(); ?>

</body>
</html>