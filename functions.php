<?php

// Add custom Theme Functions here
define('QA_PATCH', get_stylesheet_directory_uri());

require_once ('qa/boot.php');
// enqueue script qa
function custom_scripts_qa()
{
    wp_register_script('ticker-cs', QA_PATCH . '/assets/js/ticker.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('ticker-cs');
}

add_action('wp_enqueue_scripts', 'custom_scripts_qa');

// function view
function postview_set($postID) {
    $count_key = 'postview_number';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
function postview_get($postID){
    $count_key = 'postview_number';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 lượt xem";
    }
    return $count.' lượt xem';
}
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );