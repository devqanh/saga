<?php
/**
 * The blog template file.
 *
 * @package flatsome
 */

get_header();

?>
    <div id="content" class="blog-wrapper blog-archive page-wrapper">
        <div class="row">
            <div class="breadcrumb">
                <?php echo bcn_display(); ?>
            </div>
            <div class="col medium-10">
                <div id="contentPlaceHolder_subcategories">

                    <h1><?php echo single_cat_title(); ?></h1>
                    <?php if (have_posts()) : ?>

                            <?php /* Start the Loop */ ?>
                            <?php $i = 0 ; while (have_posts()) : the_post(); ?>
                            <div class="sub-cat">
                                <div class="cat-article-big cat-article-vertical <?php echo $i++==0?'highlight':'';?>">
                                    <a class="article-img"
                                       href="<?php echo get_permalink();?>">
                                        <?php the_post_thumbnail();?>
                                    </a>
                                    <div class="article-info">
                                        <a class="title invert"
                                           href="<?php echo get_permalink();?>"><?php echo get_the_title();?></a>
                                        <div class="info">
                                            <a class="author fader">THEO <?php echo get_the_author();?></a>&nbsp;&nbsp;<i class="fa fa-calendar-o" aria-hidden="true"></i> <?php echo get_the_date(); ?>
                                        </div>
                                        <p class="blurb"><?php the_excerpt();?></p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>


                            <?php endwhile; ?>

                            <?php flatsome_posts_pagination(); ?>

                    <?php endif; ?>
                </div>


            </div>
        </div>
    </div>

<?php get_footer(); ?>